﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

public class SpeedBoosterCreator : MonoBehaviour
{
    private enum Direction : byte
    {
        UP,
        DOWN
    }

    [SerializeField] private BoxCollider2D _collider;
    [SerializeField] private AreaEffector2D _areaEffector;
    [SerializeField] private GameObject _bothClosePrefab, _endOpenPrefab, _bothOpenPrefab, _startOpenPrefab;
    [SerializeField] private float _spriteWidth;
    [SerializeField] private float _bothCloseHeight, _closeToCloseHeight, _closeToOpenHeight, _bothOpenHeight;
    [SerializeField] private int _size;
    [SerializeField] private Direction _direction;
    [SerializeField] private Color32 _upColor, _downColor;

    private void OnValidate()
    {
        _collider = GetComponent<BoxCollider2D>();
        _areaEffector = GetComponent<AreaEffector2D>();
    }

    private void CreateBooster()
    {
        DestroyAllChildren(transform);
        _collider.size = Vector2.zero;
        _collider.offset = Vector2.zero;
        var position = transform.localPosition;
        switch (_size)
        {
            case 0:
                return;
            case 1:
                AddSection(_bothClosePrefab, position);
                UpdateCollider(_bothCloseHeight, false);
                UpdateEffector();
                return;
            case 2:
                position.y = AddSection(_endOpenPrefab, position, _closeToCloseHeight);
                UpdateCollider(_closeToCloseHeight);
                break;
            default:
                position.y = AddSection(_endOpenPrefab, position, _closeToOpenHeight);
                UpdateCollider(_closeToOpenHeight);
                break;
        }

        for (var i = 1; i < _size - 2; i++)
        {
            position.y = AddSection(_bothOpenPrefab, position, _bothOpenHeight);
            UpdateCollider(_bothOpenHeight);
        }

        if (_size > 2)
        {
            position.y = AddSection(_bothOpenPrefab, position, _closeToOpenHeight);
            UpdateCollider(_closeToOpenHeight);
        }

        AddSection(_startOpenPrefab, position);
        UpdateCollider(_closeToOpenHeight, false);
        UpdateEffector();
    }

    private float AddSection(GameObject prefab, Vector2 position, float spriteLength = 0)
    {
        switch (_direction)
        {
            case Direction.UP:
                Instantiate(prefab, position, Quaternion.Euler(0, 0, 90), transform).GetComponent<SpriteRenderer>().color = _upColor;
                return position.y + spriteLength;
            case Direction.DOWN:
                Instantiate(prefab, position, Quaternion.Euler(0, 0, -90), transform).GetComponent<SpriteRenderer>().color = _downColor;
                return position.y - spriteLength;
            default:
                return 0;
        }
    }

    private void UpdateCollider(float length, bool updateOffset = true)
    {
        if (updateOffset)
            switch (_direction)
            {
                case Direction.UP:
                    _collider.offset = new Vector2(0, _collider.offset.y + length / 2);
                    break;
                case Direction.DOWN:
                    _collider.offset = new Vector2(0, _collider.offset.y - length / 2);
                    break;
            }

        _collider.size = new Vector2(_spriteWidth, _collider.size.y + length);
    }

    private void UpdateEffector()
    {
        switch (_direction)
        {
            case Direction.UP:
                _areaEffector.forceAngle = 90;
                break;
            case Direction.DOWN:
                _areaEffector.forceAngle = -90;
                break;
        }
    }

    private static void DestroyAllChildren(Transform transform)
    {
        for (var i = transform.childCount; i > 0; i--)
            DestroyImmediate(transform.GetChild(0).gameObject);
    }

    [CustomEditor(typeof(SpeedBoosterCreator))]
    private class SpeedBoosterInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var speedBooster = (SpeedBoosterCreator) target;

            if (GUILayout.Button("Create Booster"))
                speedBooster.CreateBooster();
        }
    }
}
#endif