﻿using System.Collections;
using TMPro;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class PlayerController : MonoBehaviour
{
    private enum Movement : byte
    {
        NONE,
        LEFT,
        RIGHT
    }

    [SerializeField] private CameraController _cameraController;
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private float _speed;
    [SerializeField] private float _minX, _maxX;
    [SerializeField] private int _loadingDuration;
    [SerializeField] private Vector3 _homePosition, _gamePosition;
    [SerializeField] private float _smoothSpeed;
    [SerializeField] private TextMeshProUGUI _timerText, _velocityText;

    [SerializeField] [HideInInspector] private float _gravityScale;

    public GameState GameState { private get; set; }

    public Vector3 HomePosition
    {
        get { return _homePosition; }
    }

    public Vector3 GamePosition
    {
        get { return _gamePosition; }
    }

    private Movement _movement;

#if UNITY_EDITOR
    [SerializeField] private GameState _gameState;

    private void OnValidate()
    {
        _cameraController = FindObjectOfType<CameraController>();
        if (_rigidbody != null)
            _gravityScale = _rigidbody.gravityScale;
    }

    private void Update()
    {
        if (EditorApplication.isPlaying) return;
        switch (_gameState)
        {
            case GameState.HOME:
                transform.position = _homePosition;
                break;
            case GameState.LOADING:
                transform.position = _gamePosition;
                break;
            case GameState.PLAYING:
                transform.position = _gamePosition;
                break;
        }
    }
#endif

    public void Init()
    {
        GameState = GameState.HOME;
        _rigidbody.position = _homePosition;
        ResetStats();
    }

    public void Loading()
    {
        GameState = GameState.LOADING;
        _rigidbody.position = _gamePosition;
        ResetStats();
    }

    private void ResetStats()
    {
        _rigidbody.gravityScale = 0;
        _rigidbody.velocity = Vector2.zero;
        _rigidbody.angularVelocity = 0;
        _velocityText.text = "0.00";
    }

    public void Play()
    {
        StartCoroutine(StartLoading());
    }

    public void UpdatePlayer()
    {
        _movement = Movement.NONE;

        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetMouseButton(0) && Input.mousePosition.x < (float) Screen.width / 2)
            _movement = Movement.LEFT;

        if (Input.GetKey(KeyCode.RightArrow) || Input.GetMouseButton(0) && Input.mousePosition.x > (float) Screen.width / 2)
            _movement = Movement.RIGHT;

        if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow))
            _movement = Movement.NONE;
    }

    public void FixedUpdatePlayer()
    {
        switch (GameState)
        {
            case GameState.STARTING:
                _rigidbody.position = Vector2.MoveTowards(_rigidbody.position, _gamePosition, _smoothSpeed * Time.deltaTime);
                if (_rigidbody.position.x >= _gamePosition.x)
                    Play();
                break;
            case GameState.HOME:
                switch (_movement)
                {
                    case Movement.LEFT:
                        _rigidbody.AddForce(Vector2.left * _speed * Time.deltaTime);
                        break;
                    case Movement.RIGHT:
                        _rigidbody.AddForce(Vector2.right * _speed * Time.deltaTime);
                        break;
                }

                ClampX(_homePosition);
                break;
            case GameState.PLAYING:
                switch (_movement)
                {
                    case Movement.LEFT:
                        _rigidbody.AddForce(Vector2.left * _speed * Time.deltaTime);
                        break;
                    case Movement.RIGHT:
                        _rigidbody.AddForce(Vector2.right * _speed * Time.deltaTime);
                        break;
                }

                ClampX(_gamePosition);
                _velocityText.text = Mathf.Abs(_rigidbody.velocity.y).ToString("F");
                break;
        }
    }

    private void ClampX(Vector3 center)
    {
        if (_rigidbody.position.x >= center.x + _maxX || _rigidbody.position.x <= center.x + _minX)
            _rigidbody.velocity = new Vector2(0, _rigidbody.velocity.y);
        _rigidbody.position = new Vector2(Mathf.Clamp(_rigidbody.position.x, center.x + _minX, center.x + _maxX), _rigidbody.position.y);
    }

    private IEnumerator StartLoading()
    {
        _timerText.gameObject.SetActive(true);
        Loading();
        _cameraController.Loading();
        for (var i = _loadingDuration; i > 0; i--)
        {
            _timerText.text = i.ToString();
            yield return new WaitForSecondsRealtime(1);
        }

        _timerText.gameObject.SetActive(false);
        GameState = GameState.PLAYING;
        _cameraController.GameState = GameState.PLAYING;
        _rigidbody.gravityScale = _gravityScale;
        _rigidbody.velocity = Vector2.zero;
        _rigidbody.angularVelocity = 0;
    }
}