﻿using System.Collections;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class GlassController : MonoBehaviour
{
    [SerializeField] private GameController _gameController;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private BoxCollider2D _boxCollider2D;
    [SerializeField] [Range(0, 1)] private float _timeScale = .5f;
    [SerializeField] private float _slowMotionDuration = 2;
    [SerializeField] private Color32 _intactColor, _brokenColor;
    [SerializeField] private float _hpMax;
    [SerializeField] private Rigidbody2D _leftPartRigidbody, _rightPartRigidbody;
    [SerializeField] private BoxCollider2D _leftPartBoxCollider, _rightPartBoxCollider;
    [SerializeField] private SpriteRenderer _leftPartSpriteRenderer, _rightPartSpriteRenderer;

    private WaitForSecondsRealtime _wfs;
    private float _hp;

    private float HP
    {
        get { return _hp; }
        set
        {
            _hp = value;
            if (_hp > _hpMax)
                _hp = _hpMax;
            _spriteRenderer.color = Color32.Lerp(_brokenColor, _intactColor, _hp / _hpMax);
            if (_hp <= 0)
                StartCoroutine(WallBreak());
        }
    }

    private void OnValidate()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _boxCollider2D = GetComponent<BoxCollider2D>();
        _gameController = FindObjectOfType<GameController>();
    }
    
#if UNITY_EDITOR
    private void Update()
    {
        if (EditorApplication.isPlaying) return;
        _leftPartRigidbody.transform.position = new Vector3(_leftPartRigidbody.transform.position.x, transform.position.y);
        _rightPartRigidbody.transform.position = new Vector3(_rightPartRigidbody.transform.position.x, transform.position.y);
        _leftPartRigidbody.transform.localScale = new Vector3(transform.localScale.x / 2, transform.localScale.y);
        _rightPartRigidbody.transform.localScale = new Vector3(transform.localScale.x / 2, transform.localScale.y);
    }
#endif

    public void Init()
    {
        HP = _hpMax;
        InitComponents(true);
        _wfs = new WaitForSecondsRealtime(_slowMotionDuration);
        gameObject.SetActive(true);
    }

    private IEnumerator WallBreak()
    {
        InitComponents(false);
        _leftPartSpriteRenderer.color = _rightPartSpriteRenderer.color = _spriteRenderer.color;
        Time.timeScale = _timeScale;
        Time.fixedDeltaTime *= Time.timeScale;
        yield return new WaitForSecondsRealtime(_slowMotionDuration);
        _gameController.NextLevel();
    }

    private void InitComponents(bool value)
    {
        _boxCollider2D.enabled = value;
        _spriteRenderer.enabled = value;
        _leftPartRigidbody.isKinematic = _rightPartRigidbody.isKinematic = value;
        _leftPartBoxCollider.enabled = _rightPartBoxCollider.enabled = !value;
        _leftPartSpriteRenderer.enabled = _rightPartSpriteRenderer.enabled = !value;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        HP -= other.relativeVelocity.magnitude;
    }
}