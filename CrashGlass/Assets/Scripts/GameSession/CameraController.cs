﻿using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class CameraController : MonoBehaviour
{
    [SerializeField] private PlayerController _player;
    [SerializeField] private Vector3 _homeOffset, _gameOffset;
    [SerializeField] private float _smoothSpeedX, _smoothSpeedDown, _smoothSpeedUp;

    public GameState GameState { private get; set; }

#if UNITY_EDITOR
    [SerializeField] private GameState _gameState;

    private void OnValidate()
    {
        _player = FindObjectOfType<PlayerController>();
        GameState = _gameState;
    }

    private void Update()
    {
        if (EditorApplication.isPlaying) return;
        FixedUpdateCamera();
        transform.position = new Vector3(_player.transform.position.x, transform.position.y, transform.position.z);
    }
#endif

    public void Init()
    {
        transform.position = _player.HomePosition + _homeOffset;
        GameState = GameState.HOME;
    }

    public void Loading()
    {
        transform.position = new Vector3((_player.GamePosition + _homeOffset).x, transform.position.y,
            (_player.GamePosition + _homeOffset).z);
        GameState = GameState.LOADING;
    }

    public void FixedUpdateCamera()
    {
        Vector3 destination;
        switch (GameState)
        {
            case GameState.HOME:
                destination = _player.transform.position + _homeOffset;
                destination.x = transform.position.x;
                transform.position = destination;
                break;
            case GameState.STARTING:
                destination = transform.position;
                destination.x = Mathf.MoveTowards(destination.x, (_player.transform.position + _homeOffset).x,
                    _smoothSpeedX * Time.deltaTime);
                transform.position = destination;
                break;
            case GameState.LOADING:
                destination = transform.position;
                var speed = transform.position.y > (_player.transform.position + _gameOffset).y ? _smoothSpeedDown : _smoothSpeedUp;
                destination.y = Mathf.MoveTowards(destination.y, (_player.transform.position + _gameOffset).y, speed * Time.deltaTime);
                transform.position = destination;
                break;
            case GameState.PLAYING:
                destination = _player.transform.position + _gameOffset;
                destination.x = transform.position.x;
                transform.position = destination;
                break;
        }
    }
}