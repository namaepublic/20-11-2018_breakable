﻿using System.Linq;
using UnityEngine;

public class LevelController : MonoBehaviour 
{
	[SerializeField] private PlayerController _playerController;
	[SerializeField] private GlassController _glassController;

#if UNITY_EDITOR
	private void OnValidate()
	{
		_playerController = FindObjectOfType<PlayerController>();
		_glassController = GetComponentInChildren<GlassController>(true);
	}
#endif

	public void Init()
	{
		gameObject.SetActive(false);
	}

	public void StartLevel()
	{
		gameObject.SetActive(true);
		_glassController.Init();
	}


#if UNITY_EDITOR
	public static T GetComponentInChildrenWithTag<T>(Component component, string tag, bool includeInactive = false)
		where T : Component
	{
		return component.GetComponentsInChildren<T>(includeInactive).FirstOrDefault(item => item.CompareTag(tag));
	}
#endif
}
