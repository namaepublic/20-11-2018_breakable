﻿using UnityEngine;

public class UiController : MonoBehaviour
{
	[Header("Menu")]
	[SerializeField] private GameObject _menuPanel;
	[Header("Game")]
	[SerializeField] private GameObject _gamePanel;
	
	public void Init()
	{
		_menuPanel.SetActive(true);
		_gamePanel.SetActive(false);
	}

	public void GamePanel()
	{
		_menuPanel.SetActive(false);
		_gamePanel.SetActive(true);
	}

	public void MenuPanel()
	{
		_menuPanel.SetActive(true);
		_gamePanel.SetActive(false);
	}

}
