﻿using UnityEngine;
using UnityEngine.Events;

public class OnTriggerEvent : MonoBehaviour
{
	[SerializeField] private UnityEvent _event;
	
	private void OnTriggerEnter2D(Collider2D other)
	{
		if (_event != null)
			_event.Invoke();
	}
}
