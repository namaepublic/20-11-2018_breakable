﻿using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState
{
    HOME,
    STARTING,
    LOADING,
    PLAYING
}

public class GameController : MonoBehaviour
{
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private CameraController _cameraController;
    [SerializeField] private UiController _uiController;
    [SerializeField] private LevelController[] _levelControllers;

    private bool _inGame;
    
    private int _level;

    private void OnValidate()
    {
        _cameraController = FindObjectOfType<CameraController>();
        _levelControllers = GameObject.Find("World").GetComponentsInChildren<LevelController>(true);
    }

    // Use this for initialization
    void Start()
    {
        Init();
    }

    private void Init()
    {
        _inGame = false;
        _level = 0;
        _uiController.Init();
        _playerController.Init();
        _cameraController.Init();
        foreach (var levelController in _levelControllers)
            levelController.Init();
    }

    // Update is called once per frame
    void Update()
    {
        _playerController.UpdatePlayer();
    }

    private void FixedUpdate()
    {
        _playerController.FixedUpdatePlayer();
        _cameraController.FixedUpdateCamera();
    }


    public void Play()
    {
        _uiController.GamePanel();
        _playerController.GameState = GameState.STARTING;
        _cameraController.GameState = GameState.STARTING;
        NextLevel();
    }

    public void ReplayLevel()
    {
        _level--;
        NextLevel();
    }

    public void NextLevel()
    {
        Time.timeScale = 1;
        Time.fixedDeltaTime = Time.timeScale * .02f;
        if (_level >= _levelControllers.Length)
        {
            GameOver();
            return;
        }

        if (_inGame)
            _playerController.Play();
        
        foreach (var levelController in _levelControllers)
            levelController.Init();
        _levelControllers[_level++].StartLevel();
        _inGame = true;
    }

    private void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    public void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}